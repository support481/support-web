<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\LikeController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login',[LoginController::class, 'login'] );
Route::post('register',[LoginController::class, 'register'] );
Route::post('update',[UserController::class, 'update'] );
Route::get('post', [PostController::class,'index']);
Route::post('createpost',[PostController::class, 'createpost'] );
Route::post('comment',[CommentController::class, 'comment'] );
Route::post('createcomment',[CommentController::class, 'createcomment'] );
Route::post('getuser',[postController::class, 'getuser'] );
Route::post('getlike',[LikeController::class, 'getLikes'] );
Route::post('createlike',[LikeController::class, 'createlike'] );