<?php

namespace App\Http\Traits;

trait ApiTrait
{

    public function returnError($message, $errNum)
    {
        return response([
            'status' => false,
            'message' => $message
        ], $errNum);
    }

    public function returnSuccess($message = "", $data)
    {
        return response([
            'status' => true,
            'message' => $message,
            'data' => $data
        ], 200);
    }
}
