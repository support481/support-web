<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use Laravel\Passport\Client;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
class LoginController extends Controller
{
    public function register(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string',
            'phone' => 'required|string|unique:users'
        ]);
    

        if ($validator->fails()) {
            return response([
                'status' => 'error',
                'message' => $validator->messages()->first(),
            ], 400);
        }
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'phone'=> $request->phone
        ]);
        $user->save();
        $tokenResult = $user->createToken('Auth Token')->accessToken;
        return response()->json([
            'access_token' => $tokenResult->token,
            
            'user'=> $user
        ],200);
        
    }

    


    public function login(Request $request){

        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'invalid email or password'
            ], 400);
        $user = $request->user();
        $tokenResult = $user->createToken('Auth Token')->accessToken;
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        
        return response()->json([
            'access_token' => $tokenResult->token,
            'token_type' => 'Bearer',
            'user'=> $user
        ],200);
    }

    public function logout(Request $request){
        $request->user()->token()->revoke();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    public function user(Request $request){
        return response()->json($request->user());
    }
}