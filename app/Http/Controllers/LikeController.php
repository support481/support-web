<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Like;
class LikeController extends Controller
{

    public function getLikes(Request $request){

        $likes=Like::where('post_id',$request->post_id)->get();
    
        return response()->json([
            'like'=> $likes
        ],200);


    }
    public function createlike(Request $request){

        $like= new Like([
            'post_id' => $request->id,
            'user_id'=> $request->user_id,
            
        ]);

        $like->save();

        return response()->json([
            'like'=> $like
        ],200);

    }

}
