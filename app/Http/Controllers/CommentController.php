<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\comment;
use App\Models\Post;
use App\Http\Resources\commentResource;

class CommentController extends Controller
{
    public function comment(Request $post)
    {
        

        $comment = comment::where('post_id',$post->post_id)->get();
        $data = commentResource::collection($comment);

        return response()->json([
            'comments'=> $data
        ],200);
    }
    public function createcomment(Request $request){

        $post= new comment([
            'content' => $request->content,
            'user_id'=> $request->user_id,
            'post_id'=> $request->post_id,
            'reply_to' => $request->reply_to,

        ]);

        $post->save();

        return response()->json([
            'post'=> $post
        ],200);

    }

}
