<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\post;
use App\Models\User;
use App\Http\Resources\PostResource;



class PostController extends Controller
{
    public function index()
    {
        $wilayas = Post::get();
        $post = PostResource::collection($wilayas);
        return response()->json([
            'post'=> $post
        ],200);
    }
    public function getuser(Request $post){

        $user = User::where('id',$post->user_id)->get();
        return response()->json([
            'user'=> $user
        ],200);


    }
    public function createpost(Request $request){

        $post= new Post([
            'content' => $request->content,
            'user_id'=> $request->userid,
            'by_practitioner'=>$request->by_practitioner,
            'media_link'=>$request->media_link,
            'post_title'=>$request->post_title,
        ]);

        $post->save();

        return response()->json([
            'post'=> $post
        ],200);

    }
}
