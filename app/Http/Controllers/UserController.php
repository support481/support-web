<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Http\Resources\userResource;
use App\Models\User;
use App\Rules\VerifyImageBase64;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Validator;
use Kreait\Firebase\JWT\IdTokenVerifier;

class UserController extends Controller
{
    



    public function update(Request $request)
    {
        
        $user =User::find($request->id);
        
        $user->update($request->all());
        return response()->json([
            'user'=> $user
        ],200);
        
        
    
    }

    public function updateProfilePicture(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'profile_picture' => ['required', 'string', new VerifyImageBase64]
        ]);
        if ($validator->fails())
            return $this->returnError($validator->errors(), '400');

        $response = DB::transaction(function () use ($request) {
            $user = $request->user();
            $oldPicture = $user->profile_picture;
            $fs = new Filesystem();
            $intDiv = intdiv($user->id, 500);
            $folder_name = ($intDiv * 500 + 1) . '-' . ($intDiv * 500 + 500);
            $path_public = 'app/public/users/' . $folder_name . '/';
            $path_storage = storage_path($path_public);
            if (!$fs->exists($path_storage)) {
                $fs->makeDirectory($path_storage, 0755, true, false);
            }
            $name = $user->id . '-' . time() . '.' . explode(
                '/',
                explode(':', substr($request->profile_picture, 0, strpos($request->profile_picture, ';')))[1]
            )[1];

            Image::make($request->profile_picture)->resize(300, 300)->save($path_storage . $name);
            $user->profile_picture = 'storage/users/' . $folder_name . '/' . $name;
            $user->save();

            return 'success';
        });

        if ($response == 'success')
            return $this->returnSuccess('Profile picture successfully updated', ['user' => new userResource($request->user())]);
        else
            return $this->returnError('Profile picture', '500');
    }

    public function updatePhone(Request $request)
    {
        $phone = User::where([
            ['phone', '=', $request->phone],
            ['id', '=', $request->user()->id]
        ])->first();

        if ($phone)
            return $this->returnError('Phone number is the same with the old one', '422');

        $data = $request->all();
        $validator = Validator::make($data, [
            'phone' => ['required', 'digits:9', 'unique:users'],
            'firebase_token' => ['required', 'string'],
        ]);

        if ($validator->fails())
            return $this->returnError($validator->errors(), '422');

        $verifier = IdTokenVerifier::createWithProjectId(self::PROJECT_ID_FIRE_BASE);
        $payload = $verifier->verifyIdToken($request['firebase_token'])->payload();

        if ($payload == null || $payload['phone_number'] != '+213' . $request->phone)
            return $this->returnError('invalid phone number', '500');

        $user = $request->user();
        $status = $user->update([
            "phone" => $request->phone,
            'phone_verified_at' => now()
        ]);

        if ($status)
            return $this->returnSuccess('Phone number successfully updated', ['user' => new userResource($user)]);
        else
            return $this->returnError('Profile picture', '500');
    }
}

