<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Traits\ApiTrait;

class UserRequest extends FormRequest
{
    use ApiTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:255|string',
            'email' => 'required|email',
        ];
    }

    

    public function messages()
    {

        return [
            'first_name.required' => __('The first name is required'),
            'first_name.min' => __('The first name must contain at least 3 characters'),
            'first_name.max' => __('The first name must contain at most 255 characters'),
            'first_name.string' => __('The first name must contain only characters'),

            'last_name.required' => __('The last name is required'),
            'last_name.min' => __('The last name must contain at least 3 characters'),
            'last_name.max' => __('The last name must contain at most 255 characters'),
            'last_name.string' => __('The last name must contain only characters'),

            'email.required' => __('The email is required'),
            'email.email' => __('The format of the email is incorrect'),

            'address.required' => __('The address is required'),
            'address.string' => __('The address type must be string'),

            'birth_date.required' => __('The birth date is required'),
            'birth_date.date_format' => __('The birth date format is incorrect'),

            'gender.required' => __('The gender is required'),
            'gender.in' => __('Type of gender invalide'),

            'wilaya_id.required' => __('The wilaya id is required'),
            'wilaya_id.exists' => __(' The wilaya id is invalid'),

            'commune_id.required' => __('The commune id is required'),
            'commune_id.exists' => __('The commune id is invalid'),

        ];
    }
}
